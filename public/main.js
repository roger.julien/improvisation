var bc = new BroadcastChannel('gator_channel');

(() => {
  const mot = document.getElementById('mot')
  const motField = document.getElementById('mot-field')
  const equipe1 = document.getElementById('equipe1')
  const equipe1Field = document.getElementById('equipe1-field')
  const equipe2 = document.getElementById('equipe2')
  const equipe2Field = document.getElementById('equipe2-field')
  const score1 = document.getElementById('score1')
  const btnscore1 = document.getElementById('btnscore1')
  const score2 = document.getElementById('score2')
  const btnscore2 = document.getElementById('btnscore2')
  const faute11 = document.getElementById('faute11')
  const faute12 = document.getElementById('faute12')
  const faute13 = document.getElementById('faute13')
  const faute21 = document.getElementById('faute21')
  const faute22 = document.getElementById('faute22')
  const faute23 = document.getElementById('faute23')
  const btnfaute1 = document.getElementById('btnfaute1')
  const btnfaute2 = document.getElementById('btnfaute2')
  const afffaute1 = document.getElementById('afffaute1')
  const afffaute2 = document.getElementById('afffaute2')

  const setMot = (word) => {
    mot.innerHTML = word;
  }
  const setEquipe1 = (team1) => {
    equipe1.innerHTML = team1;
  }
  const setEquipe2 = (team2) => {
    equipe2.innerHTML = team2;
  }
  const setScore1 = (point1) => {
    score1.innerHTML = point1;
  }
  const setScore2 = (point2) => {
    score2.innerHTML = point2;
  }

  const setFaute1 = (faute1) => {
    faute1 = parseInt(faute1)
    afffaute1.innerHTML = faute1
    switch (faute1) {
      case 1:
        faute11.src = "./Images/FillFault.png"
        break;
      case 2:
        faute12.src = "./Images/FillFault.png"
        break;
      case 3:
        faute13.src = "./Images/FillFault.png"
        break;
      case 0:
        faute11.src = "./Images/EmptyFault.png"
        faute12.src = "./Images/EmptyFault.png"
        faute13.src = "./Images/EmptyFault.png"
        break;
    }
  }

  const setFaute2 = (faute2) => {
    faute2 = parseInt(faute2)
    afffaute2.innerHTML = faute2
    switch (faute2) {
      case 1:
        faute21.src = "./Images/FillFault.png"
        break;
      case 2:
        faute22.src = "./Images/FillFault.png"
        break;
      case 3:
        faute23.src = "./Images/FillFault.png"
        break;
      case 0:
        faute21.src = "./Images/EmptyFault.png"
        faute22.src = "./Images/EmptyFault.png"
        faute23.src = "./Images/EmptyFault.png"
        break;
      default:
        afffaute2.innerHTML = "Erreur"  

    }
  }

  localStorage.setItem('faute1', 0);
  localStorage.setItem('faute2', 0);
  setFaute1(0)
  setFaute2(0)
  setScore1(0)
  setScore2(0)

  bc.onmessage = (messageEvent) => {
    switch (messageEvent.data) {
      case 'update_mot':
        setMot(localStorage.getItem('mot'));
        break;
      case 'update_equipe1':
        setEquipe1(localStorage.getItem('equipe1'));
        break;
      case 'update_equipe2':
        setEquipe2(localStorage.getItem('equipe2'));
        break;
      case 'update_score1':
        setScore1(localStorage.getItem('score1'));
        break;
      case 'update_score2':
        setScore2(localStorage.getItem('score2'));
        break;
      case 'update_faute1':
        setFaute1(localStorage.getItem('faute1'));
        break;
      case 'update_faute2':
        setFaute2(localStorage.getItem('faute2'));
        break;
      default:
    }
  }

  btnfaute1.onclick = () => {
    var value = localStorage.getItem('faute1')
    if (value < 3) {
      value++;
    } else {
      value = 0
    }
    localStorage.setItem('faute1', value);
    setFaute1(value);
    bc.postMessage('update_faute1')
  }

  btnfaute2.onclick = () => {
    var value = localStorage.getItem('faute2')
    if (value < 3) {
      value++;
    } else {
      value = 0
    }
    localStorage.setItem('faute2', value);
    setFaute2(value);
    bc.postMessage('update_faute2')
  }

  btnscore1.onclick = () => {
    var value = parseInt(score1.innerHTML, 10);
    value++;
    localStorage.setItem('score1', value);
    score1.innerHTML = value;
    setScore1(value);
    bc.postMessage('update_score1')
  }

  btnscore2.onclick = () => {
    var value = parseInt(score2.innerHTML, 10);
    value++;
    localStorage.setItem('score2', value);
    score2.innerHTML = value;
    setScore2(value);
    bc.postMessage('update_score2')
  }

  motField.onchange = (e) => {
    const inputValue = e.target.value;
    localStorage.setItem('mot', inputValue);
    setMot(inputValue);
    bc.postMessage('update_mot');
  }

  equipe1Field.onchange = (e) => {
    const inputValue = e.target.value;
    localStorage.setItem('equipe1', inputValue);
    setEquipe1(inputValue);
    bc.postMessage('update_equipe1');
  }

  equipe2Field.onchange = (e) => {
    const inputValue = e.target.value;
    localStorage.setItem('equipe2', inputValue);
    setEquipe2(inputValue);
    bc.postMessage('update_equipe2');
  }


})()
